## CONTRIBUTING
+ Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
+ Please only open PR/Issues for a mod that is suitable for both Singleplayer and Multiplayer (no hacked clients, cheats, or any mod that modify client-side things).
+ Please do not open PR/Issues about adding OptiFabric/Optifine, you already know how bad Optifine is when compared to these open source mods included in my modpack.
+ Please do not open PR/Issues about Forge, because almost all mods used by this modpack doesn't support Forge.
+ Please do not open PR/Issues about UI changes/resource packs, stock UI is OK for most people.
