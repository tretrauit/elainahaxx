#!/usr/bin/python3

import json
import requests
import shutil
import re
import subprocess
import argparse
import toml
from urllib.parse import urlparse
from tqdm.auto import tqdm
from pathlib import Path

PACKWIZ_DIR="./src/packwiz/"
TMP=Path("./tmp/")

class CIUtils():
    def __init__(self, packwiz_path):
        self.packwiz_path = packwiz_path
        self.github_headers = {"Accept": "application/vnd.github.v3+json"}

    def _github_get_repo_commits(self, repo_dict):
        owner, repo = repo_dict["repo"].split("/")
        api_url = f"https://api.github.com/repos/{owner}/{repo}/commits?sha={repo_dict['branch']}"
        # print("Fetching information from", api_url)
        rsp = requests.get(api_url, headers=self.github_headers)
        if rsp.status_code != 200 and rsp.status_code != 301:
            rsp.raise_for_status()
            raise RuntimeError("Couldn't get GitHub commits info, return code", rsp.status_code)
        return rsp.json()

    def get_repo_commits(self, repo_dict):
        if repo_dict["type"].lower() == "github":
            return self._github_get_repo_commits(repo_dict)
        else:
            raise NotImplementedError("Only GitHub is supported for now.")

    def get_repo_latest_commit(self, repo_dict):
        commits = self.get_repo_commits(repo_dict)
        return commits[0]["sha"]

    def download_mod(self, url):
        r = requests.get(url, stream=True, allow_redirects=True)
        if r.status_code != 200:
            r.raise_for_status()  # Will only raise for 4xx codes, so...
            raise RuntimeError(f"Request to {url} returned status code {r.status_code}")

        file_size = int(r.headers.get('Content-Length', 0))

        path = TMP.joinpath(Path(urlparse(url).path).name)
        path.parent.mkdir(parents=True, exist_ok=True)

        desc = "Downloading"
        with tqdm.wrapattr(r.raw, "read", total=file_size, desc=desc) as r_raw:
            with path.open("wb") as f:
                shutil.copyfileobj(r_raw, f)

        return path

    def install_mod(self, repo, file_path):
        print("Extracting artifact...")
        unpack_dir = TMP.joinpath(Path(file_path.stem))
        unpack_dir.mkdir(parents=True, exist_ok=True)
        shutil.unpack_archive(file_path, unpack_dir)
        print("Removing existing files to prevent conflict...")
        mods_dir = self.packwiz_path.joinpath("mods")
        for file in repo["files"]:
            old_mod = mods_dir.joinpath(file)
            if old_mod.exists() and old_mod.is_file():
                old_mod.unlink()
        print("Installing...")
        regex = re.compile(repo["files_to_exclude_regex"])
        files = []
        for file in unpack_dir.glob("*.jar"):
            if regex.search(str(file)):
                continue
            print(f"Installing {file.name}...")
            shutil.copyfile(file, mods_dir.joinpath(file.name))
            files.append(file.name)
        return files

class PackWizMod():
    def __init__(self, name = None, path: Path = None, config = None):
        self.name = name
        self.path = path
        if self.path:
            if not self.name:
                self.name = self.path.name.removesuffix(''.join(self.path.suffixes))
            with self.path.open("r", encoding="utf-8") as f:
                self.packwiz = toml.load(f)
                modrinth = self.packwiz["update"].get("modrinth")
                if modrinth:
                    self.provider = "modrinth"
                    self.id = modrinth["mod-id"]
                    self.version = modrinth["version"]
                # CurseForge soon.
        else:
            self.config = config
            if not self.name:
                self.name = self.config["repo"].split("/")[1]
                self.version = self.config["repo"]["commit"]
            self.provider = "external"

    def is_packwiz(self) -> bool:
        if getattr(self, "packwiz", None):
            return True
        return False

class PackWizUtils():
    def __init__(self, packwiz = Path("./src/packwiz/"), external = Path("./repos.json")):
        self.packwiz = packwiz
        self.packwiz_mods = self.packwiz.joinpath("mods")
        self.external_json = external
        external.touch(exist_ok=True)
        self.external = json.load(external.open("r"))
        self.ci_utils = CIUtils(self.packwiz)

    def _get_default_config(self):
        return [
            {
                "repo": "<repo owner>/<repo>",
                "freezed": False,
                "branch": "master",
                "type": "github",
                "commit": "",
                "ci_url": "",
                "files_to_exclude_regex": "",
                "files": [
                    
                ]
            }
        ]

    def _write_config(self):
        json.dump(self.external, self.external_json.open("w+"), indent=4)

    def get_local_mod(self, name = None, external = True, packwiz = True, suffix = ".toml") -> PackWizMod | list[PackWizMod] | None:
        """
        Get installed mod
        + If the name is specified, it'll return the first mod with the specified name.
        + If the name is not specified, it'll return all mods
        + If `packwiz` is True, it will return the mods from the packwiz modpack.
        + If `external` is True, it will return the mods from the external config file.
        
        The return will be either a list[PackWizmod], PackWizMod (for a single mod), or None if not found.
        """
        if name is None:
            name = ""
        mods = []
        if packwiz:
            for mod in self.packwiz_mods.iterdir():
                if not suffix in mod.suffixes or mod.stem.removesuffix(".pw") != name:
                    continue
                mod = PackWizMod(path=mod)
                if name != None:
                    return mod
                mods.append(mod)
        if external:
            for mod in self.external:
                try:
                    if mod["repo"].split("/")[1] == name:
                        mod = PackWizMod(config=mod)
                        if name != None:
                            return mod
                        mods.append(mod)
                except KeyError:
                    pass
        if mods == []:
            return None
        return mods

    def _execute_packwiz(self, args):
        """
        Execute packwiz command
        """
        result = subprocess.run(["packwiz", *args], cwd=PACKWIZ_DIR)
        result.check_returncode()

    def refresh_packwiz(self):
        """
        Refresh packwiz
        """
        self._execute_packwiz(["refresh"])

    def serve_packwiz(self):
        """
        Serve packwiz
        """
        try:
            self._execute_packwiz(["serve"])
        except KeyboardInterrupt:
            pass

    def _update_mod_packwiz(self, mod):
        """
        Update mod from packwiz
        """
        self._execute_packwiz(["update", mod])

    def remove_mod_packwiz(self, name, refresh = True):
        """
        Remove mods
        """
        if isinstance(name, str) and name != "":
            name = [name]
        
        if name == [] or name == [""] or name == "":
            print("No mods specified")
            return
        
        for mod_name in name:
            mod = self.get_local_mod(mod_name)
            if not mod:
                print("Couldn't find '{}'".format(mod_name))
                continue
            if mod.is_packwiz():
                mod.path.unlink()
            else:
                for file in mod.config["files"]:
                    self.packwiz_mods.joinpath(file).unlink()
                self.external.remove(mod)
                self._write_config()
            print("Removed '{}'".format(mod_name))

        if refresh:
            self.refresh_packwiz()

    def freeze_mod(self, name, external = True, refresh = True, unfreeze = False):
        """
        Freeze/unfreeze mod
        """
        if isinstance(name, str) and name != "":
            name = [name]
        
        if name == [] or name == [""] or name == "":
            print("No mods specified")
            return

        for mod_name in name:
            suffix = ".toml"
            if unfreeze:
                suffix = ".pwfrzd"
            mod = self.get_local_mod(mod_name, external, suffix=suffix)
            if mod:
                if mod.is_packwiz():
                    new_ext = mod.path.suffix + ".pwfrzd"
                    if unfreeze:
                        new_ext = ""
                    mod.path.rename(mod.path.with_suffix(new_ext))
                    if unfreeze:
                        print("Unfreezed '{}'".format(mod.name))
                    else:
                        print("Freezed '{}'".format(mod.name))
                elif (mod.config["freezed"] and unfreeze) or (not mod.config["freezed"] and not unfreeze):
                    for i, v in enumerate(mod.config["files"]):
                        file_path = self.packwiz_mods.joinpath(v)
                        new_path = file_path.with_suffix("" if unfreeze else file_path.suffix + ".exfrzd")
                        file_path.rename(new_path)
                        mod.config["files"][i] = new_path.name
                    mod.config["freezed"] = not unfreeze
                    self._write_config()
                    if unfreeze:
                        print("Unfreezed '{}' (external)".format(mod.name))
                    else:
                        print("Freezed '{}' (external)".format(mod.name))
                else:
                    if unfreeze:
                        print(f"{mod.name} is already unfreezed")
                    else:
                        print(f"{mod.name} is already freezed")
                    refresh = False
            else:
                print(f"{mod_name} is not (un)freezed/installed.")
                refresh = False

        if refresh:
            self.refresh_packwiz()

    def install_mod(self, name, provider = "auto", force = False, refresh = True):
        """
        Install mod
        + If the name is specified and is a string, it'll install the mod with the specified name.
        + If the name is specified and is a list, it'll install all mods from the list.
        + If `provider` is either "auto", "modrinth", "mr", "curseforge", "cf" or "external" it'll install mod
        from the specified provider. If `provider` is `external`, it'll install mod from sources provided by repos.json
        + If `force` is True, do not check if mod is already installed.
        """
        if isinstance(name, str) and name != "":
            name = [name]
        
        if name == [] or name == [""] or name == "":
            print("No mods specified")
            return

        for mod_name in name:
            if mod_name == "":
                continue
            print("Installing mod '{}'...".format(mod_name))
            mod = self.get_local_mod(mod_name, external = True)
            if mod and not force:
                if mod.is_packwiz():
                    print("Mod '{}' is already installed from packwiz, updating...".format(mod.name))
                    self._update_mod_packwiz(mod.name)
                else:
                    if self.ci_utils.get_repo_latest_commit(mod.config) == mod.config["commit"]:
                        print("Mod '{}' is already installed from external, updating...".format(mod.name))
                        self.update_mod(mod.name, refresh=False)
                    elif provider == "auto" or provider == "external":
                        self.update_mod(mod.name, refresh=False)
                        print("Mod '{}' installed successfully.".format(mod.name))
                continue
            
            if provider == "auto" or provider == "mr" or provider == "modrinth":
                try:
                    print("Searching Modrinth for {}...".format(mod_name))
                    self._execute_packwiz(["mr", "install", mod_name])
                except subprocess.CalledProcessError:
                    pass
                else:
                    print("Mod '{}' installed successfully.".format(mod_name))
                    continue
            if provider == "auto" or provider == "cf" or provider == "curseforge":
                try:
                    print("Searching CurseForge for {}...".format(mod_name))
                    print("Note: Some CurseForge mods will not allow downloading from the API.")
                    self._execute_packwiz(["cf", "install", mod_name])
                except subprocess.CalledProcessError:
                    pass
                else:
                    print("Mod '{}' installed successfully.".format(mod_name))
                    continue

            print("'{}' installation failed.".format(mod_name))
                        
        if refresh:
            self.refresh_packwiz()

    def update_mod(self, name = "", external = True, reinstall = False, refresh = True):
        """
        Update mod
        + If the name is specified and is a string, it'll update the first mod with the specified name.
        + If the name is specified and is a list, it'll update all mods in the list
        + If the name is not specified, it'll update all mods
        + If `external` is True, it will update the mods from the external config file too.
        
        The return will be a list, if a object is a `Path` then it's a packwiz 
        mod, else if the object is a dict then it's a external mod.
        """
        mods = []
        if name == [] or name == ["all"]:
            name = ""
        if name == "":
            mods = self.get_local_mod(external = external, packwiz = False)
            print("Updating all packwiz mods...")
            self._update_mod_packwiz("--all")
        else:
            if isinstance(name, str):
                name = [name]
            for mod in name:
                if mod != "":
                    mods.append(self.get_local_mod(mod, external))
        if mods:
            for mod in mods:
                if not mod:
                    continue
                if mod.is_packwiz():
                    print("Updating '{}'...".format(mod.name))
                    if reinstall:
                        self.remove_mod_packwiz(mod.name, refresh=False)
                        self.install_mod(mod.name, force=True, refresh=False)
                        continue
                    self._update_mod_packwiz(mod.name)
                else:
                    print("Updating external '{}'...".format(mod.name))
                    for repo in self.external:
                        commits_info = self.ci_utils.get_repo_latest_commit(repo)
                        if commits_info == repo["commit"]:
                            print(f"{repo['repo']} is already updated.")
                            continue
                        print(f"Downloading update for '{repo['repo']}'...")
                        file = self.ci_utils.download_mod(repo["ci_url"])
                        files_installed = self.ci_utils.install_mod(repo, file)
                        repo["files"] = files_installed
                        repo["commit"] = commits_info
                        self._write_config()
                        print("Successfully updated '{}'".format(repo['repo']))
                    print("Cleaning temporary files...")
                    if TMP.exists():
                        shutil.rmtree(TMP)
                    
        if refresh:
            self.refresh_packwiz()

def main():
    modutils = PackWizUtils()
    parser = argparse.ArgumentParser(
        description="A script with some hacky utilities for PackWiz",
        epilog="To get help for each command, type the command with the --help/-h argument")
    subparser = parser.add_subparsers(dest="command", required=True, help="Execute help on each command to get help")
    update_p = subparser.add_parser('update')
    update_p.add_argument('update', nargs=argparse.REMAINDER, help="Update specified mods (or all mods if not specified)")
    update_p.add_argument('--force', nargs=argparse.REMAINDER, help="Force reinstallation of mods.")
    install_p = subparser.add_parser('install')
    install_p.add_argument('install', nargs=argparse.REMAINDER, help="Install or update specified mods if already installed (Modrinth and CurseForge)")
    reinstall_p = subparser.add_parser('reinstall')
    reinstall_p.add_argument('reinstall', nargs=argparse.REMAINDER, help="Reinstall specified mods (Modrinth and CurseForge)")
    freeze_p = subparser.add_parser('freeze')
    freeze_p.add_argument('freeze', nargs=argparse.REMAINDER, help="Disable specified mods")
    disable_p = subparser.add_parser('disable')
    disable_p.add_argument('disable', nargs=argparse.REMAINDER, help="Alias for freeze")
    unfreeze_p = subparser.add_parser('unfreeze')
    unfreeze_p.add_argument('unfreeze', nargs=argparse.REMAINDER, help="Enable specified mods if freezed")
    enable_p = subparser.add_parser('enable')
    enable_p.add_argument('enable', nargs=argparse.REMAINDER, help="Alias for unfreeze")
    serve_p = subparser.add_parser('serve')
    serve_p.add_argument('serve', nargs=argparse.REMAINDER, help="Host a local HTTP server using 'packwiz serve'")
    refresh_p = subparser.add_parser('refresh')
    refresh_p.add_argument('refresh', nargs=argparse.REMAINDER, help="Refresh the mod index")
    remove_p = subparser.add_parser('remove')
    remove_p.add_argument('remove', nargs=argparse.REMAINDER, help="Remove specificed mods")
    args = parser.parse_args()
    # print(args.command)
    update_args = getattr(args, "update", None)
    install_args = getattr(args, "install", None)
    reinstall_args = getattr(args, "reinstall", None)
    freeze_args = getattr(args, "freeze", None) or getattr(args, "disable", None)
    unfreeze_args = getattr(args, "unfreeze", None) or getattr(args, "enable", None)
    serve_args = getattr(args, "serve", None)
    refresh_args = getattr(args, "refresh", None)
    remove_args = getattr(args, "remove", None)
    if update_args or args.command == "update":
        modutils.update_mod(update_args, reinstall=("--force" in update_args))
    elif install_args or args.command == "install":
        modutils.install_mod(install_args)
    elif reinstall_args or args.command == "reinstall":
        modutils.install_mod(reinstall_args, force = True)
    elif freeze_args or args.command == "freeze" or args.command == "disable":
        modutils.freeze_mod(freeze_args)
    elif unfreeze_args or args.command == "unfreeze" or args.command == "enable":
        modutils.freeze_mod(unfreeze_args, unfreeze = True)
    elif serve_args or args.command == "serve":
        modutils.serve_packwiz()
    elif refresh_args or args.command == "refresh":
        modutils.refresh_packwiz()
    elif remove_args or args.command == "remove":
        modutils.remove_mod_packwiz(remove_args)

if __name__ == "__main__":
    main()
